const express = require('express');

const createRouter = connection => {
  const router = express.Router();

  router.get('/', (req, res) => {
    let query = 'SELECT * FROM `comments`';

    if (req.query.news_id) {
      query += ' WHERE `news_id` = ' + req.query.news_id;
    }

    connection.query(query, (error, results) => {
      if (error) {
        res.status(500).send({error: 'Database error'});
      } else {
        res.send(results);
      }
    });
  });

  router.post('/', (req, res) => {
    const data = req.body;

    connection.query('INSERT INTO `comments` (`news_id`, `author`, `text`) VALUES (?, ?, ?)',
      [data.news_id, data.author, data.text],
      (error, results) => {
        if (error) {
          res.status(500).send({error: 'Database error'});
        } else {
          res.send({
            id: results.insertId,
            ...data
          });
        }
      }
    );
  });

  router.delete('/:id', (req, res) => {
    connection.query('DELETE FROM `comments` WHERE `id` = ?', req.params.id, (error, results) => {
      if (error) {
        res.status(500).send({error: 'Database error'});
      } else {
        if (results.affectedRows) {
          res.send({message: 'Comment successfully removed from database'});
        } else {
          res.status(404).send({error: 'Not found'});
        }
      }
    });
  });

  return router;
};

module.exports = createRouter;

