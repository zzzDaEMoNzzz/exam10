const express = require('express');
const multer = require('multer');
const moment = require('moment');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const createRouter = connection => {
  const router = express.Router();

  router.get('/', (req, res) => {
    connection.query('SELECT `id`, `title`, `image`, `datetime` FROM `news`', (error, results) => {
      if (error) {
        res.status(500).send({error: 'Database error'});
      } else {
        res.send(results);
      }
    });
  });

  router.post('/', upload.single('image'), (req, res) => {
    const data = req.body;

    if (req.file) {
      data.image = req.file.filename;
    }

    data.datetime = moment().format('YYYY-MM-DD HH:mm:ss');

    connection.query('INSERT INTO `news` (`title`, `description`, `image`, `datetime`) VALUES (?, ?, ?, ?)',
      [data.title, data.description, data.image, data.datetime],
      (error, results) => {
        if (error) {
          res.status(500).send({error: 'Database error'});
        } else {
          res.send({
            id: results.insertId,
            ...data
          });
        }
      }
    );
  });

  router.get('/:id', (req, res) => {
    connection.query('SELECT * FROM `news` WHERE `id` = ?', req.params.id, (error, results) => {
      if (error) {
        res.status(500).send({error: 'Database error'});
      } else {
        if (results[0]) {
          res.send(results[0]);
        } else {
          res.status(404).send({error: 'Not found'});
        }
      }
    });
  });

  router.delete('/:id', (req, res) => {
    connection.query('DELETE FROM `news` WHERE `id` = ?', req.params.id, (error, results) => {
      if (error) {
        res.status(500).send({error: 'Database error'});
      } else {
        if (results.affectedRows) {
          res.send({message: 'News successfully removed from database'});
        } else {
          res.status(404).send({error: 'Not found'});
        }
      }
    });
  });

  return router;
};

module.exports = createRouter;

