import React, {Component} from 'react';
import {connect} from 'react-redux';
import {deletePost, getNews} from "../../store/actions";
import Post from "../../components/Post/Post";
import './News.css';

class News extends Component {
  addBtnHandler = () => this.props.history.push('/add');

  componentDidMount() {
    this.props.getNews();
  }

  render() {
    const newsList = this.props.news.map(post => (
      <Post
        key={post.id}
        id={post.id}
        title={post.title}
        image={post.image}
        datetime={post.datetime}
        handler={() => this.props.deletePost(post.id)}
      />
    ));

    return (
      <div className="News">
        <h2>
          Posts
          <button onClick={this.addBtnHandler}>Add new post</button>
        </h2>
        {newsList}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  news: state.news
});

const mapDispatchToProps = dispatch => ({
  getNews: () => dispatch(getNews()),
  deletePost: id => dispatch(deletePost(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(News);