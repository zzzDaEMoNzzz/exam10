import React, {Component} from 'react';
import {connect} from 'react-redux';
import {addPost} from "../../store/actions";
import './NewsAdd.css';

class NewsAdd extends Component {
  state = {
    title: '',
    description: '',
    image: null
  };

  inputOnChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileOnChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    });
  };

  formOnSubmitHandler = event => {
    event.preventDefault();

    const formData = new FormData();

    Object.keys(this.state).forEach(key => {
      if (this.state[key]) formData.append(key, this.state[key]);
    });

    this.props.addPost(formData).then(() => {
      this.setState({
        title: '',
        description: '',
        image: null
      });

      this.props.history.push('/');
    });
  };

  render() {
    return (
      <form className="NewsAdd" onSubmit={this.formOnSubmitHandler}>
        <h2>Add new post</h2>
        <label>Title</label>
        <input
          type="text"
          name="title"
          value={this.state.title}
          onChange={this.inputOnChangeHandler}
        />
        <label>Content</label>
        <textarea
          rows="10"
          name="description"
          value={this.state.description}
          onChange={this.inputOnChangeHandler}
        />
        <label>Image</label>
        <input
          type="file"
          name="image"
          onChange={this.fileOnChangeHandler}
        />
        <button>Save</button>
      </form>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  addPost: data => dispatch(addPost(data))
});

export default connect(null, mapDispatchToProps)(NewsAdd);