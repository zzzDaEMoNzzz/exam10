import React, {Component} from 'react';
import {connect} from "react-redux";
import {addComment, deleteComment, getPostInfo} from "../../store/actions";
import './NewsInfo.css';
import Comment from "../../components/Comment/Comment";

class NewsInfo extends Component {
  state = {
    author: '',
    text: ''
  };

  postID = this.props.match.params.id;

  componentDidMount() {
    this.props.getPostInfo(this.postID);
  }

  addCommentHandler = event => {
    event.preventDefault();

    this.props.addComment(this.postID, this.state.author, this.state.text).then(() => {
      this.setState({
        text: ''
      });
    });
  };

  inputOnChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  deleteCommentHandler = id => {
    this.props.deleteComment(id).then(() => this.props.getPostInfo(this.postID));
  };

  render() {
    const commentsList = this.props.postInfo.comments.map(item => (
      <Comment
        key={item.id}
        author={item.author}
        comment={item.text}
        handler={() => this.deleteCommentHandler(item.id)}
      />
    ));

    return (
      <div className="NewsInfo">
        <h2>{this.props.postInfo.title || 'Loading...'}</h2>
        <p className="NewsInfo-date">at {this.props.postInfo.datetime || ''}</p>
        <p className="NewsInfo-description">{this.props.postInfo.description || ''}</p>
        <h3>Comments</h3>
        {commentsList}
        <h3>Add comment</h3>
        <form onSubmit={this.addCommentHandler}>
          <label>Name</label>
          <input
            type="text"
            name="author"
            value={this.state.author}
            onChange={this.inputOnChangeHandler}
          />
          <label>Comment</label>
          <input
            type="text"
            name="text"
            value={this.state.text}
            onChange={this.inputOnChangeHandler}
            required
          />
          <button>Add</button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  postInfo: state.postInfo,
});

const mapDispatchToProps = dispatch => ({
  getPostInfo: id => dispatch(getPostInfo(id)),
  addComment: (news_id, author, text) => dispatch(addComment(news_id, author, text)),
  deleteComment: id => dispatch(deleteComment(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(NewsInfo);