import {GET_NEWS_SUCCESS, GET_POST_INFO_SUCCESS} from "./actions";

const initialState = {
  news: [],
  postInfo: {
    title: '',
    description: '',
    datetime: '',
    comments: [],
  },
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_NEWS_SUCCESS:
      return {...state, news: action.posts};
    case GET_POST_INFO_SUCCESS:
      return {...state, postInfo: action.postInfo};
    default:
      return state;
  }
};

export default reducer;