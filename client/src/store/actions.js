import axios from 'axios';

export const GET_NEWS_SUCCESS = 'GET_NEWS_SUCCESS';
export const GET_POST_INFO_SUCCESS = 'GET_POST_INFO_SUCCESS';

export const getNewsSuccess = posts => ({type: GET_NEWS_SUCCESS, posts});

export const getNews = () => {
  return dispatch => {
    axios.get('/news').then(
      response => dispatch(getNewsSuccess(response.data))
    );
  }
};

export const addPost = data => {
  return dispatch => {
    return axios.post('/news', data).then(() => {
      dispatch(getNews());
    });
  }
};

export const getPostInfoSuccess = postInfo => ({type: GET_POST_INFO_SUCCESS, postInfo});

export const getPostInfo = id => {
  return dispatch => {
    axios.get('/news/' + id).then(responseNews => {
      const postData = responseNews.data;

      axios.get('/comments/?news_id=' + id).then(responseComments => {
        const postComments = responseComments.data;
        const postInfo = {
          ...postData,
          comments: postComments
        };

        dispatch(getPostInfoSuccess(postInfo));
      });
    });
  }
};

export const deletePost = id => {
  return dispatch => {
    axios.delete('/news/' + id).then(() => {
      dispatch(getNews());
    });
  }
};

export const addComment = (news_id, author, text) => {
  return dispatch => {
    return axios.post('/comments', {
      news_id,
      author,
      text
    }).then(() => dispatch(getPostInfo(news_id)));
  }
};

export const deleteComment = id => {
  return dispatch => {
    return axios.delete('/comments/' + id);
  }
};