import React, {Component} from 'react';
import './Comment.css';


class Comment extends Component {
  render() {
    return (
      <div className="Comment">
        <span><b>{this.props.author || 'Anonymous'}</b> wrote:</span>
        <span className="Comment-text">{this.props.comment}</span>
        <button onClick={this.props.handler}>Delete</button>
      </div>
    );
  }
}

export default Comment;