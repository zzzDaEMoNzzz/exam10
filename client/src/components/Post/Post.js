import React from 'react';
import {Link} from "react-router-dom";
import {apiUrl} from "../../constants";
import './Post.css';

const Post = ({id, title, datetime, image, handler}) => {
  let imageElement = null;
  if (image) imageElement  = <img src={apiUrl + '/uploads/' + image} alt=""/>;

  return (
    <div className="Post">
      {imageElement}
      <div className="Post-body">
        <h4 className="Post-title">{title}</h4>
        <div className="Post-info">
          <span>At {datetime}</span>
          <Link to={'/news/' + id}>Read Full Post >></Link>
          <button onClick={handler}>Delete</button>
        </div>
      </div>
    </div>
  );
};

export default Post;