import React from 'react';
import {Link} from "react-router-dom";
import './Header.css';

const Header = () => {
  return (
    <header className="Header">
      <nav>
        <Link to="/">News</Link>
      </nav>
    </header>
  );
};

export default Header;