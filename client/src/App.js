import React, { Component } from 'react';
import {Route, Switch} from "react-router-dom";
import News from "./containers/News/News";
import NewsAdd from "./containers/NewsAdd/NewsAdd";
import NewsInfo from "./containers/NewsInfo/NewsInfo";
import Header from "./components/Header/Header";
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
        <div className="App-body">
          <Switch>
            <Route path="/" exact component={News}/>
            <Route path="/news" exact component={News}/>
            <Route path="/add" exact component={NewsAdd}/>
            <Route path="/news/:id" exact component={NewsInfo}/>
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
